#!/bin/bash

. ./config.sh

ruby generate.rb $EMOJI

for i in ${AP_IP[@]}; do
  sshpass -p $SSH_PASS scp $SSH_USER@$i:/usr/etc/syswrapper.sh ./
  sed -i '' '/emoji/d' syswrapper.sh
  sed -i '' '/\. \/usr\/etc\/common.sh/ a\
  . /usr/etc/fixemoji.sh
  ' syswrapper.sh
  sed -i '' '/cfg_save() {/ a\
  	fix_emoji /tmp/system.cfg
  ' syswrapper.sh
  sshpass -p $SSH_PASS scp ./syswrapper.sh $SSH_USER@$i:/usr/etc/syswrapper.sh
  sshpass -p $SSH_PASS scp ./.fixemoji $SSH_USER@$i:/usr/etc/fixemoji.sh
  sshpass -p $SSH_PASS ssh $SSH_USER@$i '/usr/etc/syswrapper.sh apply-config'
done

sleep 15
for i in ${AP_IP[@]}; do
  sshpass -p $SSH_PASS ssh $SSH_USER@$i 'killall hostapd'
done

rm syswrapper.sh
rm .fixemoji
