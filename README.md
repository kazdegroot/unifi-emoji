# Fix wrong emoji codepoints in Unifi config

The process moving configuration files to unifi ap's replaces 4 octet UTF sequences with 6 octet ones.  
Those aren't Unicode compliant 😕  
As the rest of the system seems to have no issue dealing with 4 octet UTF sequences, fixing the configfile after it's installed but before it's parsed works perfectly.

However, persistence seems to be an issue, so we need to do this after every reboot. But soft reboots can be survived!

First config by copying `config.sh.dist` to `config.sh` and filling out the parameters.
SSH_USER and SSH_PASS tend to be your unifi admin credentials.
AP_IP is a list of all AP ipadresses (like (10.0.0.1 10.0.0.2) etc.)  
EMOJI is a sequence of all the emoji you (might) want to use.  

You should probably chown this file to your user, and chmod it 600.

Running `fix.sh` will download `syswrapper.sh`, patch it to call the fix emoji script, then upload everything and call `syswrapper.sh apply-config`.

## sshpass

Only use for this! We can't persist an ssh key on the AP so, we use this to automate logging in.

`brew install https://raw.githubusercontent.com/kadwanev/bigboybrew/master/Library/Formula/sshpass.rb`

## generator

`generate.rb` generates a `.fixemoji` file, based on either the parameters on the command line, or the defaults set within
